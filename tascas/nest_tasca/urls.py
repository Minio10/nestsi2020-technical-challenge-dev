from django.urls import path
from . import views

urlpatterns = [

    path("",views.index,name = "index"),

    path("create", views.create,name = "create"),

    path("new_tasca",views.new_tasca,name = "new_tasca"),

    path("delete_tasca/<int:id>",views.delete_tasca,name = "delete_tasca"),

    path("edit_tasca_page/<int:id>",views.edit_tasca_page,name="edit_tasca_page"),

    path("edit_tasca",views.edit_tasca,name ="edit_tasca"),

    path("search",views.search,name="search"),

    path("see_tasca/<int:id>",views.see_tasca,name="see_tasca"),
    
    path("sort_by_rating",views.sort_by_rating,name="sort_by_rating")
]
