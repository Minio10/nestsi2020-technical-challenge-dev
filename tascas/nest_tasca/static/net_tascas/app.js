
var latitude;
var longitude;


function geocode(location){
  axios.get('https://maps.googleapis.com/maps/api/geocode/json?',{
    params:{
      address:location,
      key:'AIzaSyD_CABT_sxyUu0GdDf7t1e4ueRv6XNyIkU'
    }
  })
  .then(function(response){
    console.log(response);
    
    latitude = response.data.results[0].geometry.location.lat;
    longitude = response.data.results[0].geometry.location.lng;
    initMap(latitude,longitude);

  })
  .catch(function(error){
    console.log(error);
  })
}

function initMap(latitude,longitude){

  var options = {
    zoom:11,
    center: {lat : latitude, lng : longitude}

  }
  var map = new google.maps.Map(document.getElementById('map'),options);

  //add marker
  var marker = new google.maps.Marker({
    position: {lat : latitude, lng : longitude},
    map:map
  });
}
