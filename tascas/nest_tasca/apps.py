from django.apps import AppConfig


class NestTascaConfig(AppConfig):
    name = 'nest_tasca'
