from django.db import models


# Create your models here.

class Tasca(models.Model):

    name = models.CharField(max_length= 100)
    address = models.CharField(max_length= 100)
    rating = models.IntegerField()
    image_link = models.CharField(max_length=700, default=None, blank=True, null=True)
