from django.shortcuts import render,redirect
from django.http import HttpResponse,HttpResponseRedirect
from django import forms
from .models import Tasca
from django.urls import reverse

#Form for creating/edting a Tasca
class NewTascaForm(forms.Form):

    name = forms.CharField(widget = forms.Textarea(
        attrs = {
            'class': 'form-control'
        }
    ))

    address = forms.CharField(widget = forms.Textarea(
        attrs = {
            'class': 'form-control'
        }
    ))

    rating = forms.IntegerField(label = "Rating 1-10",min_value=1, max_value=10,widget = forms.TextInput(
        attrs = {
            'class': 'form-control','type':'number'
        }
    ))

    url = forms.CharField(label = "Tasca Image URL",widget = forms.TextInput(
        attrs = {
            'class': 'form-control'
        }
    ))

def index(request):

    #Get all tascas
    all_tascas = Tasca.objects.all()

    #Returns the template with all tascas
    return render(request, "nest_tasca/index.html",{
        "tascas":all_tascas
    })

def create(request):

    #sends the form to create a tasca
    return render(request,"nest_tasca/create.html",{
        "form":NewTascaForm()
    })

def new_tasca(request):

    if request.method == "POST":
        form = NewTascaForm(request.POST)

        # Verifies if the form is filled in the correct way
        if form.is_valid():
            name = form.cleaned_data["name"]
            address = form.cleaned_data["address"]
            rating = form.cleaned_data["rating"]
            url = form.cleaned_data["url"]

            #Checks if the Tasca already exists by checking the name and the address

            if (Tasca.objects.filter(name = name,address = address)):
                return render(request,"nest_tasca/create.html",{
                    "form":NewTascaForm(),"message":"That tasca already exists!"
                })

            #Adds the Tasca to the DB
            new_tasca = Tasca()
            new_tasca.name = name
            new_tasca.address = address
            new_tasca.rating = rating
            new_tasca.image_link = url
            new_tasca.save()

        #Redirects the user to the Index Template
        else:
            return render(request,"nest_tasca/create.html",{
                "form":form,"message": "An error occurred while trying to add the new Tasca. Please try again!"
            })
        return HttpResponseRedirect(reverse("index"))

        #Error Case
    return render(request,"nest_tasca/error.html")

def delete_tasca(request,id):

    # returns the selected Tasca
    tasca = Tasca.objects.get(id=id)

    #Deletes the selected tasca
    tasca.delete()

    #Redirects the user to the Index Template
    return HttpResponseRedirect(reverse("index"))

def edit_tasca_page(request, id):

    #Gets the Tasca that will be edited
    tasca = Tasca.objects.get(id=id)

    #Prepopulates the form with the stored info
    form = NewTascaForm({"name": tasca.name, "address": tasca.address,"rating": tasca.rating,"url":tasca.image_link})

    return render(request,"nest_tasca/edit.html",{
        "form":form,"tasca_id":id
    })

def edit_tasca(request):

    if request.method == "POST":
        form = NewTascaForm(request.POST)

        tasca_id = request.POST.get("tasca_id")

        tasca_pre_edit = Tasca.objects.get(id = tasca_id)

        # Verifies if the form is filled in the correct way
        if form.is_valid():
            name = form.cleaned_data["name"]
            address = form.cleaned_data["address"]
            rating = form.cleaned_data["rating"]
            url = form.cleaned_data["url"]

            #Check if any info was changed
            if(tasca_pre_edit.name == name and tasca_pre_edit.address == address and tasca_pre_edit.rating == rating and tasca_pre_edit.image_link == url):
                return render(request,"nest_tasca/edit.html",{
                    "form":form,"message":"You didnt change any parameter. Please try again!"
                })

            #Checks if the Tasca already exists
            tasca = Tasca.objects.filter(id = tasca_id)

            #Updates the values of the tasca
            tasca.update(name = name)
            tasca.update(address = address)
            tasca.update(rating = rating)
            tasca.update(image_link = url)

        #Redirects the user to the Index Template
        return HttpResponseRedirect(reverse("index"))

    return render(request,"nest_tasca/error.html");

def search(request):

    if request.method == "GET":
        word = request.GET['word']

        #Searches addresses or Names
        tascas = Tasca.objects.filter(name__icontains=word) | Tasca.objects.filter(address__icontains = word)

        #If there is at least one match it returns the results
        if(tascas):
            return render(request, "nest_tasca/search_results.html",{
                "tascas":tascas
            })
        #Else returns a message
        return render(request, "nest_tasca/search_results.html",{
            "message":"There were no results that matched your search word"
        })

def see_tasca(request,id):

    # returns the selected Tasca
    tasca = Tasca.objects.get(id=id)

    return render(request,"nest_tasca/tasca.html",{
        "tasca":tasca
    })

def sort_by_rating(request):

    all_tascas = Tasca.objects.none()

    if request.method == "POST":

        option = request.POST.get("sort")

        #Sorts the tascas using desceding order highest to lowest
        if option == "1":

            all_tascas = Tasca.objects.all().order_by("-rating")

        #Sorts the tascas using ascending order lowest to highest
        elif option == "0":

            all_tascas = Tasca.objects.all().order_by("rating")

        return render(request, "nest_tasca/index.html",{
            "tascas":all_tascas
        })

    return render(request, "nest_tasca/error.html")
